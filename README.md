# Samaritan web

This project is a web-based implementation of [PoI](https://www.imdb.com/title/tt1839578/)'s Samaritan UI.

Disclaimer : like in the show, this Samaritan is thick.

## Getting started

### Prerequisites

- PC
- Google Chrome
- Microphone

*No other software than official Google's web browser will support voice recognition.*

### Installing

Just copy browse the files with your browser or host the files in any web browser.

## Built with

Web components :
- [JavaScript Cookie](https://github.com/js-cookie/js-cookie) - JS cookie API
- [True visibility](https://github.com/UseAllFive/true-visibility) - Checks if a DOM element is truly visible

## Authors

**KaKi87 (Tiana Lemesle)** - *Initial work*

## Contributors

### Animation design

- [Elarson](https://www.youtube.com/channel/UCxtnba6n4iMFFZ_k2ld2LRA/)

### Inspiration

- [lukesaltweather](https://github.com/lukesaltweather)
- [rodrigograca31](https://github.com/rodrigograca31)
- isoleucine

## License
This project is licensed under the GPLv2 license - see the [LICENSE](LICENSE) file for details